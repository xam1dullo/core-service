
import { Controller } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { RabbitService } from './rabbit.service';

@Controller('rabbit')
export class RabbitController {
  constructor(private readonly rabbitService: RabbitService) { }

  @EventPattern('test-pattern')
  async handleTestPattern(data: any) {
    try {
      console.log('Received message from RabbitMQ:', data);
      return await this.rabbitService.sendMessageToTelegram(data.message);
    } catch (error) {
    }
  }
}
