import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { ClientProxyFactory, Transport, ClientProxy } from '@nestjs/microservices';
import { ConfigService } from '@nestjs/config';
import { Bot } from 'grammy';

@Injectable()
export class RabbitService implements OnModuleInit {
  private client: ClientProxy;
  private readonly bot: Bot;
  private readonly chatId: string;
  private readonly logger = new Logger(RabbitService.name);

  constructor(private configService: ConfigService) {
    const amqpUrl = this.configService.get<string>('amqp.url');

    if (!amqpUrl) {
      throw new Error('RABBITMQ_URL is not defined in the environment variables');
    }
    this.client = ClientProxyFactory.create({
      transport: Transport.RMQ,
      options: {
        urls: [amqpUrl],
        queue: 'main_queue',
        queueOptions: {
          durable: true,
        },
      },
    });

    const botToken = this.configService.get<string>('TELEGRAM_BOT_TOKEN');
    if (!botToken) {
      throw new Error('TELEGRAM_BOT_TOKEN is not defined in the environment variables');
    }
    this.bot = new Bot(botToken);

    const chatId = this.configService.get<string>('TELEGRAM_CHAT_ID');
    if (!chatId) {
      throw new Error('TELEGRAM_CHAT_ID is not defined in the environment variables');
    }
    this.chatId = chatId;

  }

  async onModuleInit() {
    try {
      await this.client.connect();
    } catch (error) {
      this.logger.error('Failed to connect to RabbitMQ', error);
    }
  }

  async sendMessageToTelegram(message: string) {
    try {
      return await this.bot.api.sendMessage(this.chatId, message);
    } catch (error) {
      this.logger.error('Failed to send message to Telegram', error);
      throw error; // Xatoni qayta tashlash
    }
  }
}
