import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Transport, MicroserviceOptions } from '@nestjs/microservices';
import { join } from 'path';
import { Logger } from '@nestjs/common';
import { config } from "dotenv";
import { COURSE_PACKAGE_NAME } from './common/types';

config()

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const grpcMicroservice = app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.GRPC,
    options: {
      package: COURSE_PACKAGE_NAME,
      protoPath: join(__dirname, '..', '..', 'globals', 'proto', 'core.proto'),
      url: process.env.GRPC_CORE_SERVICE
    }
  });

  const rmqMicroservice = app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.RMQ,
    options: {
      urls: ['amqp://localhost:5672'],
      queue: 'main_queue',
      queueOptions: {
        durable: true,
      },
    },
  });
  await app.startAllMicroservices();
  await app.listen(3004);

  Logger.log(`Application is running }`)
}

bootstrap();
