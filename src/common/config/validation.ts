import * as Joi from 'joi';

export const validationSchema = Joi.object({
  PORT: Joi.number().default(3000),
  GRPC_AUTH_SERVICE: Joi.string().required(),
  GRPC_CORE_SERVICE: Joi.string().required(),
  REDIS_HOST: Joi.string().required(),
  REDIS_PORT: Joi.number().required(),
  RABBITMQ_URL: Joi.string().required(),
  JWT_SECRET: Joi.string().required(),
});
