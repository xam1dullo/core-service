
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CourseEntity } from './entity/course.entity';
import { Course, CreateCourseRequest, CreateCourseResponse, DeleteCourseRequest, DeleteCourseResponse, GetAllCoursesResponse, GetCourseByIdRequest, GetCourseByIdResponse, UpdateCourseRequest, UpdateCourseResponse } from '@app/common/types';


@Injectable()
export class CoursesService {
  constructor(
    @InjectRepository(CourseEntity)
    private readonly courseRepository: Repository<CourseEntity>,
  ) { }

  async createCourse(request: CreateCourseRequest): Promise<CreateCourseResponse> {
    const course = this.courseRepository.create(request);
    const savedCourse = await this.courseRepository.save(course);
    return { message: 'Course created successfully' };
  }

  async updateCourse(request: UpdateCourseRequest): Promise<UpdateCourseResponse> {
    await this.courseRepository.update({ id: +request.id }, { ...request, id: +request.id });
    return { message: 'Course updated successfully' };
  }

  async deleteCourse(request: DeleteCourseRequest): Promise<DeleteCourseResponse> {
    await this.courseRepository.delete(+request.id);
    return { message: 'Course deleted successfully' };
  }

  async getAllCourses(): Promise<GetAllCoursesResponse> {
    const courses = await this.courseRepository.find({});
    return { courses: courses.map(this.toCourse) };
  }

  async getCourseById(request: GetCourseByIdRequest): Promise<GetCourseByIdResponse> {
    const course = await this.courseRepository.findOne({ where: { id: +request.id } });
    return { course: this.toCourse(course) };
  }

  private toCourse(courseEntity: CourseEntity): Course {
    return {
      id: courseEntity.id.toString(),
      title: courseEntity.title,
    };
  }
}
