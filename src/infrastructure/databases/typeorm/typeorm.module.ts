import { Global, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmProvider } from "./typeorm.providers";

@Global()
@Module({
  imports: [
    ConfigModule,
    TypeOrmModule.forRootAsync(typeOrmProvider),
  ],
  exports: [TypeOrmModule],
})
export class TypeOrmDatabaseModule {}
