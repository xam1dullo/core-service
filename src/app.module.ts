import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ThrottlerModule } from '@nestjs/throttler';
import { InfrastructureModule } from './infrastructure/infrastructure.module';
import { CourseModule } from './modules/courses/courses.module';
import { configuration, validationSchema } from "./common";
import { RabbitModule } from './modules/rabbit/rabbit.module';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [configuration],
      validationSchema,
    }),
    ThrottlerModule.forRoot([
      {
        ttl: 600000,
        limit: 100,
      },
    ]),
    ClientsModule.register([
      {
        name: 'RABBITMQ_SERVICE',
        transport: Transport.RMQ,
        options: {
          urls: [process.env.RABBITMQ_URL],
          queue: 'main_queue',
          queueOptions: {
            durable: true,
          },
        },
      },
    ]),
    CourseModule,
    InfrastructureModule,
    RabbitModule,
  ],
  providers: [],
  controllers: [],
})
export class AppModule { }
