import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { ConfigService, ConfigModule } from '@nestjs/config';
import { CoursesService } from './courses.service';
import { CoursesController } from './courses.controller';
import { JwtStrategy } from "@app/common";
import { TypeOrmModule } from '@nestjs/typeorm';
import { CourseEntity } from './entity/course.entity';
import { DatabasesModule } from '@Infra/databases/databases.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([CourseEntity]),
    PassportModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get<string>('JWT_SECRET'),
        signOptions: { expiresIn: configService.get<number>('JWT_EXPIRATION') },
      }),
      inject: [ConfigService],
    }),
    ConfigModule,
    DatabasesModule,
  ],
  controllers: [CoursesController],
  providers: [
    CoursesService,
    JwtStrategy,
  ],
})
export class CourseModule { }
