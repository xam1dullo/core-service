import { Observable } from 'rxjs';
import { CreateCourseDto, UpdateCourseDto } from '../dto';

export interface CourseServiceInterface {
  create(data: CreateCourseDto): Observable<any>;
  update(data: UpdateCourseDto): Observable<any>;
  delete(data: { id: string }): Observable<any>;
  findAll(): Observable<any>;
  findById(data: { id: string }): Observable<any>;
}
