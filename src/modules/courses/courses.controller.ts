import { Controller } from '@nestjs/common';
import { CoursesService } from './courses.service';
import {
  CourseServiceControllerMethods,
  CourseServiceController,
  CreateCourseRequest,
  UpdateCourseRequest,
  DeleteCourseRequest,
  GetAllCoursesRequest,
  GetCourseByIdRequest,
  CreateCourseResponse,
  UpdateCourseResponse,
  DeleteCourseResponse,
  GetAllCoursesResponse,
  GetCourseByIdResponse
} from '@app/common/types';

@Controller()
@CourseServiceControllerMethods()
export class CoursesController implements CourseServiceController {
  constructor(private readonly coursesService: CoursesService) { }


  createCourse(request: CreateCourseRequest): Promise<CreateCourseResponse> {
    return this.coursesService.createCourse(request);
  }


  updateCourse(request: UpdateCourseRequest): Promise<UpdateCourseResponse> {
    return this.coursesService.updateCourse(request);
  }


  deleteCourse(request: DeleteCourseRequest): Promise<DeleteCourseResponse> {
    return this.coursesService.deleteCourse(request);
  }



  getAllCourses(): Promise<GetAllCoursesResponse> {
    return this.coursesService.getAllCourses();
  }


  getCourseById(request: GetCourseByIdRequest): Promise<GetCourseByIdResponse> {
    return this.coursesService.getCourseById(request);
  }
}
